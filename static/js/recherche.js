$(document).ready(function(){
  $('#valider').click(function(){
    var elems = $('form').find(':input');
    var qsElems = [];
    console.log(elems);
    for(var i=0; i<elems.length;i++){
      if((elems[i].tagName == "INPUT" || elems[i].tagName == "SELECT") && elems[i].value){
        qsElems.push({"name":elems[i].name, "val":elems[i].value});
      }
    }
    // console.log(qsElems);
    // console.log(makeQueryString(qsElems));
    window.location = "films.php?"+makeQueryString(qsElems)
  });
});


function makeQueryString(qsElems){
  var qs = "";
  for(var i=0; i<qsElems.length;i++){
    qs = qs+qsElems[i].name+"="+qsElems[i].val;
    if(i != qsElems.length-1){qs = qs+"&";}
  }
  return qs;
}
